from repository.BaseRepository import BaseRepository


class CustomerRepository(BaseRepository):
    def __init__(self):
        super().__init__(table='customers')
