from repository.EmployeeRepository import EmployeeRepository


class EmployeeService:
    def __init__(self):
        self.employee_repository = EmployeeRepository()

    def get_employees(self):
        result = self.employee_repository.find_all()
        if result is not None:
            return [obj.__dict__ for obj in result]
        else:
            return None

    def get_employee_by(self, **kwargs):
        result = self.employee_repository.find_by(**kwargs)
        if result is not None:
            return result.__dict__
        else:
            return None
