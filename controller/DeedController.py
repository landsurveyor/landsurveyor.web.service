import datetime
import logging
import os
import pprint
import uuid
from pathlib import Path

from flask import Blueprint, request, jsonify, abort
from kanpai import Kanpai

import config
from model.Deed import DeedRequest, DeedStatusEnum
from model.Image import Image, ImageRequest
from service.DeedService import DeedService
from service.ImageService import ImageService

deed_controller = Blueprint('deed_controller', __name__)
deed_service = DeedService()
image_service = ImageService()


@deed_controller.route('/api/v1/deeds', methods=['GET'])
def get_deeds():
    schema = Kanpai.Object({
        'customer_id': Kanpai.String(),
        'employee_id': Kanpai.String(),
        'status_id': Kanpai.String(),
        'sort': Kanpai.String(),
        'limit': Kanpai.String()
    })
    validation_result = schema.validate(request.args.to_dict())
    if validation_result.get('success', False) is False:
        logging.error(validation_result.get('error'))
        abort(400)
    if len(request.args) == 0:
        deeds = deed_service.get_deeds()
        if deeds is None:
            return jsonify({}), 204
        return jsonify(deeds)
    else:
        deed = None
        try:
            deed = deed_service.get_deeds_by(**request.args)
        except Exception as e:
            logging.error(e)
            abort(404)
        if deed is None or len(deed) == 0:
            abort(404)
        return jsonify(deed), 200


@deed_controller.route('/api/v1/deeds', methods=['POST'])
def create_deed():
    pprint.pprint(('BODY', request.form.to_dict()))
    if 'image' in request.files:
        pprint.pprint(('FILE', request.files.getlist('image')))
    schema = Kanpai.Object({
        'customer_id': Kanpai.String().required(),
        'deed_name': Kanpai.String().required(),
        'address': Kanpai.String(),
        'nearby_landmark': Kanpai.String(),
        'latitude': Kanpai.String().required(),
        'longitude': Kanpai.String().required()
    })
    validation_result = schema.validate(request.form.to_dict())
    if validation_result.get('success', False) is False:
        logging.error(validation_result.get('error'))
        abort(400)

    image_list = []

    if 'image' in request.files:
        for image in request.files.getlist('image'):
            if image.content_type in config.ALLOWED_IMAGE_FORMATS:
                ext = image.filename.split('.')[-1]
                image_name = str(uuid.uuid4()) + '.' + ext
                image_path = ('/images/' + image_name)
                image_list.append(Image(image=image, name=image_name, path=image_path))
            else:
                logging.error('Image type invalid')
                abort(400)

    # Initial status to waiting
    status_id = DeedStatusEnum.WAITING.value
    # Initial created date to now
    created_date = datetime.datetime.now()

    # Create DeedRequest object to pass data to service
    deed = DeedRequest(customer_id=request.form['customer_id'],
                       employee_id=None,
                       deed_name=request.form['deed_name'],
                       address=request.form['address'],
                       nearby_landmark=request.form['nearby_landmark'],
                       latitude=request.form['latitude'],
                       longitude=request.form['longitude'],
                       area=None,
                       deed_number=None,
                       deed_type_id=None,
                       status_id=status_id,
                       created_date=created_date,
                       updated_date=None)

    # Check image directory exist
    # If not exist create directory
    root_dir = Path(__file__).parent.parent
    image_dir = os.path.join(root_dir, 'images')
    if not os.path.exists(image_dir):
        os.mkdir(image_dir)

    try:
        # Save new deed request to database
        deed_id = deed_service.create_deed(deed)
        if deed_id is None:
            abort(400)
        if image_list is not None:
            for image in image_list:
                # Save image object to storage
                image.image.save(os.path.join(image_dir, image.name))
                # Save image path to database
                image_service.create_image(ImageRequest(deed_id=deed_id, image_path=image.path))
        return jsonify(message='success'), 201
    except Exception as e:
        logging.error(e)
        abort(500)


@deed_controller.route('/api/v1/deeds/<int:deed_id>', methods=['PATCH'])
def update_deed(deed_id):
    pprint.pprint(('BODY', request.json))
    schema = Kanpai.Object({
        'employee_id': Kanpai.Number(),
        'area': Kanpai.Number(),
        'deed_number': Kanpai.String(),
        'deed_type_id': Kanpai.Number(),
        'status_id': Kanpai.Number()
    })
    validation_result = schema.validate(request.json)
    if validation_result.get('success', False) is False:
        logging.error(validation_result.get('error'))
        abort(400)

    updated_date = datetime.datetime.now()

    if request.json['status_id'] is not None:
        status_mapping = dict((item.value, item) for item in DeedStatusEnum)
        if request.json['status_id'] not in status_mapping:
            abort(400)

        # current_deed = deed_service.get_deeds_by_id(deed_id)
        # if not check_deed_status(current_status=current_deed['status'], update_status=request.json['status']):
        #     abort(400)

    deed = DeedRequest(customer_id=None,
                       employee_id=request.json['employee_id'],
                       deed_name=None,
                       address=None,
                       nearby_landmark=None,
                       latitude=None,
                       longitude=None,
                       area=request.json['area'],
                       deed_number=request.json['deed_number'],
                       deed_type_id=request.json['deed_type_id'],
                       status_id=request.json['status_id'],
                       created_date=None,
                       updated_date=updated_date)
    try:
        deed_service.update_deed(deed_id, deed)
        return jsonify(message='success')
    except ValueError as e:
        logging.error(e)
        abort(400)
    except Exception as e:
        logging.error(e)
        abort(500)


@deed_controller.route('/api/v1/deeds/<int:deed_id>', methods=['GET'])
def get_deeds_by_id(deed_id):
    deed = deed_service.get_deeds_by_id(deed_id)
    if deed is None:
        abort(404)
    return jsonify(deed)


@deed_controller.route('/api/v1/deeds/counter', methods=['GET'])
def get_status_counter():
    return jsonify(deed_service.count_all_status())


def check_deed_status(current_status: str, update_status: str) -> bool:
    if current_status == DeedStatusEnum.WAITING.value:
        if update_status in [DeedStatusEnum.ASSIGNED.value, DeedStatusEnum.CANCELLED.value]:
            return True
    elif current_status == DeedStatusEnum.ASSIGNED.value:
        if update_status in [DeedStatusEnum.COLLECTING.value, DeedStatusEnum.CANCELLED.value]:
            return True
    elif current_status == DeedStatusEnum.COLLECTING.value:
        if update_status in [DeedStatusEnum.SUCCESS.value]:
            return True
    else:
        return False
