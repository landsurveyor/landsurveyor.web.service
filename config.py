APP_NAME_CUSTOMER = 'Land Requester'
APP_NAME_EMPLOYEE = 'Land Surveyor'

MAX_FILE_IMAGE = 5
MAX_IMAGE_SIZE = 5242880
ALLOWED_IMAGE_FORMATS = ['image/jpg', 'image/jpeg']
