from repository.BaseRepository import BaseRepository


class ImageRepository(BaseRepository):
    def __init__(self):
        super().__init__(table='images')
