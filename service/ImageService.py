from model.Image import ImageRequest
from repository.ImageRepository import ImageRepository


class ImageService:
    def __init__(self):
        self.image_repository = ImageRepository()

    def create_image(self, image_request: ImageRequest):
        return self.image_repository.insert(**image_request.__dict__)
