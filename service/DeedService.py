from model.Deed import DeedResponse, DeedRequest, DeedCounter, DeedStatusEnum
from repository.CustomerRepository import CustomerRepository
from repository.DeedRepository import DeedRepository
from repository.DeedStatusRepository import DeedStatusRepository
from repository.DeedTypeRepository import DeedTypeRepository
from repository.EmployeeRepository import EmployeeRepository
from repository.ImageRepository import ImageRepository


class DeedService:
    def __init__(self):
        self.deed_repository = DeedRepository()
        self.deed_status_repository = DeedStatusRepository()
        self.deed_type_repository = DeedTypeRepository()
        self.customer_repository = CustomerRepository()
        self.employee_repository = EmployeeRepository()
        self.image_repository = ImageRepository()

    def build_response(self, result):
        deed_response_list = []
        if isinstance(result, list):
            deed_list = result
        else:
            deed_list = [result]

        for deed in deed_list:
            customer = self.customer_repository.find_by(customer_id=deed.customer_id).__dict__
            if deed.employee_id is not None:
                employee = self.employee_repository.find_by(employee_id=deed.employee_id).__dict__
            else:
                employee = None
            images = self.image_repository.find_by(deed_id=deed.deed_id)
            if images is not None:
                image_path_list = [image.image_path for image in images] \
                    if isinstance(images, list) else [images.image_path]
            else:
                image_path_list = None
            if deed.deed_type_id is not None:
                deed_type = self.deed_type_repository.find_by(deed_type_id=deed.deed_type_id).__dict__
            else:
                deed_type = None
            if deed.status_id is not None:
                status = self.deed_status_repository.find_by(status_id=deed.status_id)
            else:
                status = None

            deed_response_list.append(
                DeedResponse(deed_id=deed.deed_id,
                             customer=customer,
                             employee=employee,
                             deed_name=deed.deed_name,
                             image_path=image_path_list,
                             address=deed.address,
                             nearby_landmark=deed.nearby_landmark,
                             latitude=deed.latitude,
                             longitude=deed.longitude,
                             area=deed.area,
                             deed_number=deed.deed_number,
                             deed_type=deed_type,
                             status_id=status.status_id,
                             created_date=deed.created_date,
                             updated_date=deed.updated_date))
        if len(deed_response_list) == 1:
            return deed_response_list[0].__dict__
        else:
            return [obj.__dict__ for obj in deed_response_list]

    def get_deeds(self):
        result = self.deed_repository.find_all()
        if result is not None:
            response = self.build_response(result)
            if isinstance(response, dict):
                return [response]
            else:
                return response
        else:
            return None

    def get_deeds_by_id(self, deed_id: int):
        result = self.deed_repository.find_by(deed_id=deed_id)
        if result is not None:
            return self.build_response(result)
        else:
            return None

    def get_deeds_by(self, **kwargs):
        result = self.deed_repository.find_by(**kwargs)
        if result is not None:
            response = self.build_response(result)
            if isinstance(response, dict):
                return [response]
            else:
                return response
        else:
            return None

    def create_deed(self, deed_request: DeedRequest):
        row_id = None
        customer = self.customer_repository.find_by(customer_id=deed_request.customer_id)
        if customer is not None:
            row_id = self.deed_repository.insert(**deed_request.__dict__)
        return row_id

    def update_deed(self, deed_id: int, deed_request: DeedRequest):
        deed = self.get_deeds_by_id(deed_id)
        if deed is not None:
            request = dict()
            request['deed_id'] = deed_id
            if deed_request.employee_id is not None:
                employee = self.employee_repository.find_by(employee_id=deed_request.employee_id)
                if employee is not None:
                    request['employee_id'] = deed_request.employee_id
                else:
                    raise ValueError(
                        '400 Bad Request: The browser (or proxy) sent a request that this server could not understand.')
            if deed_request.area is not None:
                request['area'] = deed_request.area
            if deed_request.deed_number is not None:
                request['deed_number'] = deed_request.deed_number
            if deed_request.deed_type_id is not None:
                deed_type = self.deed_type_repository.find_by(deed_type_id=deed_request.deed_type_id)
                if deed_type is not None:
                    request['deed_type_id'] = deed_request.deed_type_id
                else:
                    raise ValueError(
                        '400 Bad Request: The browser (or proxy) sent a request that this server could not understand.')
            if deed_request.status_id is not None:
                request['status_id'] = deed_request.status_id
            if deed_request.updated_date is not None:
                request['updated_date'] = deed_request.updated_date
            self.deed_repository.update(**request)

    def count_all_status(self):
        total = self.deed_repository.count_deeds()
        waiting = self.deed_repository.count_deeds_by_status(DeedStatusEnum.WAITING.value)
        assigned = self.deed_repository.count_deeds_by_status(DeedStatusEnum.ASSIGNED.value)
        collecting = self.deed_repository.count_deeds_by_status(DeedStatusEnum.COLLECTING.value)
        success = self.deed_repository.count_deeds_by_status(DeedStatusEnum.SUCCESS.value)
        cancelled = self.deed_repository.count_deeds_by_status(DeedStatusEnum.CANCELLED.value)

        deed_counter = DeedCounter(total, waiting, assigned, collecting, success, cancelled)
        return deed_counter.__dict__
