from repository.CustomerRepository import CustomerRepository


class CustomerService:
    def __init__(self):
        self.customer_repository = CustomerRepository()

    def get_customers(self):
        result = self.customer_repository.find_all()
        if result is not None:
            return [obj.__dict__ for obj in result]
        else:
            return None

    def get_customer_by(self, **kwargs):
        result = self.customer_repository.find_by(**kwargs)
        if result is not None:
            return result.__dict__
        else:
            return None
