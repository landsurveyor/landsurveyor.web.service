from model.Deed import DeedStatus
from repository.DeedStatusRepository import DeedStatusRepository


class DeedStatusService:
    def __init__(self):
        self.deed_status_repository = DeedStatusRepository()

    def get_deed_status(self):
        result = self.deed_status_repository.find_all()
        if result is not None:
            return [obj.__dict__ for obj in result]
        else:
            return None

    def get_deed_status_role(self, role):
        result = self.deed_status_repository.find_all()
        status_response = []
        if role == 'customer':
            for status in result:
                status_response.append(
                    DeedStatus(status_id=status.status_id,
                               name=status.name_customer))
            return [obj.__dict__ for obj in status_response]
        elif role == 'employee':
            for status in result:
                if status.status_id == 3:
                    status_response.append(
                        DeedStatus(status_id=status.status_id,
                                   name=status.name_employee))
            for status in result:
                if status.status_id == 2:
                    status_response.append(
                        DeedStatus(status_id=status.status_id,
                                   name=status.name_employee))
            for status in result:
                if status.status_id == 4:
                    status_response.append(
                        DeedStatus(status_id=status.status_id,
                                   name=status.name_employee))
            return [obj.__dict__ for obj in status_response]
        elif role == 'admin':
            for status in result:
                status_response.append(
                    DeedStatus(status_id=status.status_id,
                               name=status.name_admin))
            return [obj.__dict__ for obj in status_response]
        else:
            return None
