import logging
import pprint

from flask import Blueprint, request, jsonify, abort
from kanpai import Kanpai

from service.UserService import UserService

user_controller = Blueprint('user_controller', __name__)
user_service = UserService()


@user_controller.route('/api/v1/login', methods=['POST'])
def login():
    pprint.pprint(('BODY', request.json))
    schema = Kanpai.Object({
        "username": Kanpai.String().required(),
        "password": Kanpai.String().required(),
        "role": Kanpai.String().required()
    })
    validation_result = schema.validate(request.json)
    if validation_result.get('success', False) is False:
        logging.error(validation_result.get('error'))
        abort(400)
    try:
        user_id = user_service.login(**request.json)
        if user_id is None:
            return jsonify(message='incorrect username or password'), 422
        else:
            return jsonify(user_id=user_id), 200
    except Exception as e:
        logging.error(e)
        abort(500)
