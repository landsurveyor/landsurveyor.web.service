from repository.BaseRepository import BaseRepository


class DeedStatusRepository(BaseRepository):
    def __init__(self):
        super().__init__(table='status')
