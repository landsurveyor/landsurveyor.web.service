from repository.DeedTypeRepository import DeedTypeRepository


class DeedTypeService:
    def __init__(self):
        self.deed_type_repository = DeedTypeRepository()

    def get_deed_types(self):
        result = self.deed_type_repository.find_all()
        if result is not None:
            return [obj.__dict__ for obj in result]
        else:
            return None

    def get_deed_types_by(self, **kwargs):
        result = self.deed_type_repository.find_by(**kwargs)
        if result is not None:
            return result.__dict__
        else:
            return None
