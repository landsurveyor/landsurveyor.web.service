DROP TABLE IF EXISTS `land_surveyor`.`images`;
DROP TABLE IF EXISTS `land_surveyor`.`deeds`;
DROP TABLE IF EXISTS `land_surveyor`.`deed_types`;
DROP TABLE IF EXISTS `land_surveyor`.`employees`;
DROP TABLE IF EXISTS `land_surveyor`.`customers`;
DROP TABLE IF EXISTS `land_surveyor`.`status`;
DROP TABLE IF EXISTS `land_surveyor`.`users`;

create table users
(
    user_id  int auto_increment
        primary key,
    username varchar(50) not null,
    password varchar(50) not null,
    role     varchar(50) not null
);

create table deed_types
(
    deed_type_id int auto_increment
        primary key,
    name         varchar(100) null
);

create table customers
(
    customer_id  int auto_increment
        primary key,
    user_id      int          not null,
    first_name   varchar(100) null,
    last_name    varchar(100) null,
    phone_number varchar(15)  null,
    constraint customers_users_user_id_fk
        foreign key (user_id) references users (user_id)
);

create table employees
(
    employee_id int auto_increment
        primary key,
    user_id     int          not null,
    first_name  varchar(100) null,
    last_name   varchar(100) null,
    constraint employees_users_user_id_fk
        foreign key (user_id) references users (user_id)
);

create table status
(
    status_id     int auto_increment
        primary key,
    name          varchar(100) null,
    name_customer varchar(100) null,
    name_employee varchar(100) null,
    name_admin    varchar(100) null
);

create table deeds
(
    deed_id         int auto_increment
        primary key,
    customer_id     int             null,
    employee_id     int             null,
    deed_name       varchar(100)    null,
    address         varchar(100)    null,
    nearby_landmark varchar(100)    null,
    latitude        decimal(15, 12) null,
    longitude       decimal(15, 12) null,
    area            decimal(30, 12) null,
    deed_number     varchar(45)     null,
    deed_type_id    int             null,
    status_id       int             null,
    created_date    datetime        null,
    updated_date    datetime        null,
    constraint deeds_customers_customer_id_fk
        foreign key (customer_id) references customers (customer_id),
    constraint deeds_deed_types_deed_type_id_fk
        foreign key (deed_type_id) references deed_types (deed_type_id),
    constraint deeds_employees_employee_id_fk
        foreign key (employee_id) references employees (employee_id),
    constraint deeds_status_status_id_fk
        foreign key (status_id) references status (status_id)
);

create table images
(
    image_id   int auto_increment
        primary key,
    deed_id    int          not null,
    image_path varchar(200) null,
    constraint images_deeds_deed_id_fk
        foreign key (deed_id) references deeds (deed_id)
);

INSERT INTO `land_surveyor`.`deed_types` (deed_type_id, name)
VALUES ('1', 'สค.1'),
       ('2', 'นส.2'),
       ('3', 'นส.3'),
       ('4', 'นส.3ก'),
       ('5', 'นส.4');

INSERT INTO `land_surveyor`.`status` (status_id, name, name_customer, name_employee, name_admin)
    VALUE ('1', 'waiting', 'waiting', 'waiting', 'waiting'),
    ('2', 'assigned', 'assigned', 'assigned', 'assigned'),
    ('3', 'collecting', 'collecting', 'collecting', 'collecting'),
    ('4', 'success', 'success', 'success', 'success'),
    ('5', 'cancelled', 'cancelled', 'cancelled', 'cancelled');

INSERT INTO `land_surveyor`.`users` (username, password, role)
VALUES ('joelnwza', '123456', 'customer'),
       ('chickzaa', '1234', 'customer'),
       ('johnyw', '1234', 'employee'),
       ('admin', 'admin', 'admin'),
       ('chanont', '123456', 'customer');

INSERT INTO `land_surveyor`.`customers` (user_id, first_name, last_name, phone_number)
VALUES ('1', 'Kachapat', 'Buakhaw', '0917324427'),
       ('2', 'Phanuwich', 'Phumee', '0910322509'),
       ('5', 'Chanon', 'Treemeth', '0806956194');

INSERT INTO `land_surveyor`.`employees` (user_id, first_name, last_name)
VALUES ('3', 'Johny', 'Walker'),
       ('4', 'Admin', 'admin');

INSERT INTO `land_surveyor`.`deeds` (`customer_id`, `employee_id`, `deed_name`, `address`, `nearby_landmark`,
                                     `latitude`, `longitude`,
                                     `area`, `deed_number`, `deed_type_id`, `status_id`, `created_date`, `updated_date`)
VALUES ('1', '1', 'มศว', '114 ซอย สุขุมวิท 23 แขวง คลองเตยเหนือ เขตวัฒนา กรุงเทพมหานคร 10110', 'Terminal 21',
        '13.746002794889', '100.565496205702', '144000', 'เลขที่ 101', '1', '4', '2020-02-20 00:00:00',
        '2020-02-20 00:00:00'),
       ('1', '1', 'วิศวิบูลย์', '214 เขตดินแดง กรุงเทพมหานคร 10400', 'Big C Extra Ratchada', '13.771360558015',
        '100.569472114323', null, null, null, '2', '2020-02-20 00:00:00', null),
       ('2', '1', 'The Street Ratchada', '139 ถนนรัชดาภิเษก แขวง ดินแดง เขตดินแดง กรุงเทพมหานคร 10400',
        'Big C Extra Ratchada', '13.770397196490', '100.57230779074', null, null, null, '3',
        '2020-02-20 00:00:00', '2020-02-20 00:00:00'),
       ('2', null, 'Central Rama 9', '9 ถนนรัชดาภิเษก แขวง ห้วยขวาง เขตห้วยขวาง กรุงเทพมหานคร 10320', 'G Tower',
        '13.758697381742', '100.566236830666', null, null, null, '1', '2020-02-20 00:00:00', null),
       ('3', null, 'test', '', null, '6.4', '2.4', null, null, null, '5', '2020-02-20 00:00:00',
        '2020-02-20 00:00:00'),
       ('3', '1', 'test', '', null, '6.4', '2.4', null, null, null, '5', '2020-02-20 00:00:00',
        '2020-02-20 00:00:00');
