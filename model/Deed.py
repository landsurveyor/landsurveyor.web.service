import datetime
from enum import Enum
from typing import List


class DeedStatusEnum(Enum):
    WAITING = 1
    ASSIGNED = 2
    COLLECTING = 3
    SUCCESS = 4
    CANCELLED = 5


class DeedStatus(object):
    def __init__(self, status_id: int, name: str):
        self.status_id = status_id
        self.name = name


class DeedRequest(object):
    def __init__(self, customer_id: int, employee_id: int, deed_name: str, address: str, nearby_landmark: str,
                 latitude: float, longitude: float, area: float, deed_number: str, deed_type_id: int, status_id: int,
                 created_date: datetime, updated_date: datetime):
        self.customer_id = customer_id
        self.employee_id = employee_id
        self.deed_name = deed_name
        self.address = address
        self.nearby_landmark = nearby_landmark
        self.latitude = latitude
        self.longitude = longitude
        self.area = area
        self.deed_number = deed_number
        self.deed_type_id = deed_type_id
        self.status_id = status_id
        self.created_date = created_date
        self.updated_date = updated_date


class DeedResponse(object):
    def __init__(self, deed_id: int, customer: dict, employee: dict, deed_name: str, image_path: List[str],
                 address: str, nearby_landmark: str, latitude: float, longitude: float, area: float, deed_number: str,
                 deed_type: object, status_id: int, created_date: datetime, updated_date: datetime):
        self.deed_id = deed_id
        self.customer = customer
        self.employee = employee
        self.deed_name = deed_name
        self.image_path = image_path
        self.address = address
        self.nearby_landmark = nearby_landmark
        self.latitude = latitude
        self.longitude = longitude
        self.area = area
        self.deed_number = deed_number
        self.deed_type = deed_type
        self.status_id = status_id
        self.created_date = created_date
        self.updated_date = updated_date


class DeedCounter(object):
    def __init__(self, total: int, waiting: int, assigned: int, collecting: int, success: int, cancelled: int):
        self.total = total
        self.waiting = waiting
        self.assigned = assigned
        self.collecting = collecting
        self.success = success
        self.cancelled = cancelled
