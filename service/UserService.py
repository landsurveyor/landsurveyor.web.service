from repository.UserRepository import UserRepository


class UserService:
    def __init__(self):
        self.user_repository = UserRepository()

    def get_user_role(self, **kwargs):
        result = self.user_repository.find_by(**kwargs)
        if result is not None:
            return result.role
        else:
            return None

    def login(self, **kwargs):
        result = self.user_repository.find_by(**kwargs)
        if result is not None:
            return result.user_id
        else:
            return None
