import pprint

from flask import Flask, jsonify, send_from_directory
from flask_cors import CORS
from flask_swagger_ui import get_swaggerui_blueprint

from controller.ConfigController import config_controller
from controller.CustomerController import customer_controller
from controller.DeedController import deed_controller
from controller.DeedTypeController import deed_type_controller
from controller.EmployeeController import employee_controller
from controller.UserController import user_controller


class LoggingMiddleware(object):
    def __init__(self, app):
        self._app = app

    def __call__(self, environ, resp):
        errorlog = environ['wsgi.errors']
        pprint.pprint(('REQUEST', environ), stream=errorlog)

        def log_response(status, headers, *args):
            pprint.pprint(('RESPONSE', status, headers), stream=errorlog)
            return resp(status, headers, *args)

        return self._app(environ, log_response)


application = Flask(__name__)
application.config['JSON_SORT_KEYS'] = False
application.config['UPLOAD_FOLDER'] = 'images'
CORS(application, resources={r"/api/*": {"origins": "*"}})

application.register_blueprint(deed_controller)
application.register_blueprint(deed_type_controller)
application.register_blueprint(user_controller)
application.register_blueprint(employee_controller)
application.register_blueprint(customer_controller)
application.register_blueprint(config_controller)

# swagger specific
SWAGGER_URL = '/api/swagger'
API_URL = '/static/swagger.yaml'
SWAGGER_UI_BLUEPRINT = get_swaggerui_blueprint(
    SWAGGER_URL,
    API_URL,
    config={
        'app_name': "Land Surveyor Web Service"
    }
)
application.register_blueprint(SWAGGER_UI_BLUEPRINT, url_prefix=SWAGGER_URL)


@application.route('/images/<path:image_name>', methods=['GET'])
def get_image(image_name):
    return send_from_directory('images', image_name)


@application.errorhandler(400)
def bad_request(error):
    return jsonify(message='bad request'), 400


@application.errorhandler(404)
def not_found(error):
    return jsonify(message='not found'), 404


@application.errorhandler(500)
def internal_error(error):
    return jsonify(message='internal server error'), 500


if __name__ == '__main__':
    application.wsgi_app = LoggingMiddleware(application.wsgi_app)
    application.run(host='0.0.0.0', port=5000, debug=True)
