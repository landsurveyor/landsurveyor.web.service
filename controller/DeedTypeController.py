from flask import Blueprint, jsonify, abort

from service.DeedTypeService import DeedTypeService

deed_type_controller = Blueprint('deed_type_controller', __name__)
deed_type_service = DeedTypeService()


@deed_type_controller.route('/api/v1/deedType', methods=['GET'])
def get_deed_types():
    deed_type = deed_type_service.get_deed_types()
    if deed_type is None:
        return jsonify({}), 204
    return jsonify(deed_type)


@deed_type_controller.route('/api/v1/deedType/<int:deed_type_id>', methods=['GET'])
def get_deed_type_by_id(deed_type_id):
    deed_type = deed_type_service.get_deed_types_by(deed_type_id=deed_type_id)
    if deed_type is None:
        abort(404)
    return jsonify(deed_type)
