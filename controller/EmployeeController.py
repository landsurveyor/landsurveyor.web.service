import logging

from flask import Blueprint, request, jsonify, abort
from kanpai import Kanpai

from service.EmployeeService import EmployeeService

employee_controller = Blueprint('employee_controller', __name__)
employee_service = EmployeeService()


@employee_controller.route('/api/v1/employees', methods=['GET'])
def get_employee():
    schema = Kanpai.Object({
        'user_id': Kanpai.String(),
    })
    validation_result = schema.validate(request.args.to_dict())
    if validation_result.get('success', False) is False:
        logging.error(validation_result.get('error'))
        abort(400)
    if len(request.args) == 0:
        employees = employee_service.get_employees()
        if employees is None:
            return jsonify({}), 204
        return jsonify(employees)
    elif len(request.args) == 1:
        employee = None
        if 'user_id' in request.args:
            user_id = int(request.args['user_id'])
            employee = employee_service.get_employee_by(user_id=user_id)
        if employee is None:
            abort(404)
        return jsonify(employee), 200


@employee_controller.route('/api/v1/employees/<int:employee_id>', methods=['GET'])
def get_employee_by_id(employee_id):
    employee = employee_service.get_employee_by(employee_id=employee_id)
    if employee is None:
        abort(404)
    return jsonify(employee)
