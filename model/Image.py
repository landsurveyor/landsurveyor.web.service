class Image:
    def __init__(self, image: object, name: str, path: str):
        self.image = image
        self.name = name
        self.path = path


class ImageRequest:
    def __init__(self, deed_id: int, image_path: str):
        self.deed_id = deed_id
        self.image_path = image_path
