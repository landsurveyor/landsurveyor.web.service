from repository.BaseRepository import BaseRepository


class DeedTypeRepository(BaseRepository):
    def __init__(self):
        super().__init__(table='deed_types')
