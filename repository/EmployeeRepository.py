from repository.BaseRepository import BaseRepository


class EmployeeRepository(BaseRepository):
    def __init__(self):
        super().__init__(table='employees')
