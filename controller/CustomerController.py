import logging

from flask import Blueprint, request, jsonify, abort
from kanpai import Kanpai

from service.CustomerService import CustomerService

customer_controller = Blueprint('customer_controller', __name__)
customer_service = CustomerService()


@customer_controller.route('/api/v1/customers', methods=['GET'])
def get_customer():
    schema = Kanpai.Object({
        'user_id': Kanpai.String(),
    })
    validation_result = schema.validate(request.args.to_dict())
    if validation_result.get('success', False) is False:
        logging.error(validation_result.get('error'))
        abort(400)
    if len(request.args) == 0:
        customer = customer_service.get_customers()
        if customer is None:
            return jsonify({}), 204
        return jsonify(customer)
    elif len(request.args) == 1:
        customer = None
        if 'user_id' in request.args:
            user_id = int(request.args['user_id'])
            customer = customer_service.get_customer_by(user_id=user_id)
        if customer is None:
            abort(404)
        return jsonify(customer), 200


@customer_controller.route('/api/v1/customers/<int:customer_id>', methods=['GET'])
def get_customer_by_id(customer_id):
    customer = customer_service.get_customer_by(customer_id=customer_id)
    if customer is None:
        abort(404)
    return jsonify(customer)
