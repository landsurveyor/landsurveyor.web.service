from repository.BaseRepository import BaseRepository


class DeedRepository(BaseRepository):
    def __init__(self):
        super().__init__(table='deeds')

    def get_result_counter(self, query: str) -> int:
        super().connect()
        result = super().execute(query)
        super().close_connection()
        if len(result) > 0:
            return result[0]['COUNT(deed_id)']
        else:
            return 0

    def count_deeds(self):
        query = "SELECT COUNT(deed_id) FROM deeds"
        return self.get_result_counter(query)

    def count_deeds_by_status(self, status_id: int):
        query = "SELECT COUNT(deed_id) FROM deeds WHERE status_id='%s'" % str(status_id)
        return self.get_result_counter(query)
