import logging
import config

from flask import Blueprint, request, jsonify, abort
from kanpai import Kanpai

from service.CustomerService import CustomerService
from service.DeedStatusService import DeedStatusService
from service.DeedTypeService import DeedTypeService
from service.EmployeeService import EmployeeService
from service.UserService import UserService

config_controller = Blueprint('config_controller', __name__)
customer_service = CustomerService()
employee_service = EmployeeService()
deed_status_service = DeedStatusService()
deed_type_service = DeedTypeService()
user_service = UserService()


@config_controller.route('/api/v1/configs', methods=['GET'])
def get_config():
    schema = Kanpai.Object({
        'user_id': Kanpai.String().required(),
    })
    validation_result = schema.validate(request.args.to_dict())
    if validation_result.get('success', False) is False:
        logging.error(validation_result.get('error'))
        abort(400)
    try:
        user_id = int(request.args['user_id'])
    except ValueError as e:
        logging.error(e)
        abort(400)
        return

    role = user_service.get_user_role(user_id=user_id)
    if role == 'customer':
        user = customer_service.get_customer_by(user_id=user_id)
        status = deed_status_service.get_deed_status_role(role)
        resource = dict(deeds=dict(url='/api/v1/deeds'),
                        status=status)
        response = dict(app_name=config.APP_NAME_CUSTOMER,
                        user=user,
                        resources=resource,
                        maximum_file_image=config.MAX_FILE_IMAGE,
                        maximum_image_size=config.MAX_IMAGE_SIZE,
                        allowed_image_formats=config.ALLOWED_IMAGE_FORMATS)
        return jsonify(response)
    elif role == 'employee':
        user = employee_service.get_employee_by(user_id=user_id)
        status = deed_status_service.get_deed_status_role(role)
        deed_type = deed_type_service.get_deed_types()
        resource = dict(deeds=dict(url='/api/v1/deeds'),
                        status=status,
                        deed_type=deed_type)
        response = dict(app_name=config.APP_NAME_EMPLOYEE,
                        user=user,
                        resources=resource)
        return jsonify(response)
    elif role == 'admin':
        user = employee_service.get_employee_by(user_id=user_id)
        status = deed_status_service.get_deed_status_role(role)
        resource = dict(deeds=dict(url='/api/v1/deeds'),
                        employees=dict(url='/api/v1/employees'),
                        status=status)
        response = dict(app_name=config.APP_NAME_EMPLOYEE,
                        user=user,
                        resources=resource)
        return jsonify(response)
    else:
        return abort(404)
